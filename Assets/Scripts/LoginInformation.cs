﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LoginInformation : MonoBehaviour
{

    public TextMeshProUGUI name;

    private void Awake()
    {
        if (PlayerPrefs.GetString("login") != null && PlayerPrefs.GetString("old") != null)
        {
            name.SetText(PlayerPrefs.GetString("login"));
        }
    }
    public void NameChanged(string login)
    {
        name.SetText(login);
        PlayerPrefs.SetString("login", login);
    }
    public void AgeChanged(string old)
    {
        PlayerPrefs.SetString("old", old);
        Debug.Log(PlayerPrefs.GetString("old"));
    }
}
