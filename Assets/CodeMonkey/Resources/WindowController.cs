﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class WindowController : MonoBehaviour
{
    [SerializeField]
    GameObject personalRecomendations;
    [SerializeField]
    GameObject elderlyScreen;
    [SerializeField]
    GameObject[] elderlyStepsScreen;
    [SerializeField]
    GameObject youngScreen;
    [SerializeField]
    GameObject[] youngStepsScreen;

    [SerializeField]
    GameObject spreadScreen;

    [SerializeField]
    GameObject LoginScreen;

    [SerializeField]
    GameObject symptomsScreen;
    [SerializeField]
    GameObject allOkayScreen;
    [SerializeField]
    GameObject needDoctorScreen;
    [SerializeField]
    GameObject[] symptomsScreenSteps;

    [SerializeField]
    GameObject protectScreen;
    [SerializeField]
    GameObject[] protectScreenSteps;

    [SerializeField]
    GameObject infectedScreen;
    [SerializeField]
    GameObject[] infectedStepsScreen;

    [SerializeField]
    GameObject[] progressBarInfected;
    [SerializeField]
    GameObject[] progressBarProtect;
    [SerializeField]
    GameObject[] progressBarTest;
    [SerializeField]
    GameObject[] progressBarRecomendation;
    [SerializeField]
    GameObject homeScreen;
    int InfectionCounter = -1;
    int ProtectCounter = -1;
    int SymptomsCounter = -1;
    int elderlyCounter = -1;
    int youngCounter = -1;

    int symptomsAnswerCounter = 0;

    private void Awake()
    {
        if (PlayerPrefs.GetString("login") != "" && PlayerPrefs.GetString("old") != "")
        {
            LoginScreen.SetActive(false);
            homeScreen.SetActive(true);
        }
        else
        {
            LoginScreen.SetActive(true);
            homeScreen.SetActive(false);
        }

    }

    public void ElderlyTestSlyder()
    {
        if (elderlyCounter == 2)
        {
            elderlyCounter = 0;
            homeScreen.SetActive(true);
            elderlyScreen.SetActive(false);
            personalRecomendations.SetActive(false);
        }
        elderlyCounter++;
        elderlyStepsScreen[elderlyCounter].SetActive(true);
        elderlyStepsScreen[elderlyCounter - 1].SetActive(false);
        progressBarRecomendation[elderlyCounter].SetActive(true);
        progressBarRecomendation[elderlyCounter - 1].SetActive(false);
    }
    public void YoungTestSlyder()
    {
        if (youngCounter == 2)
        {
            youngCounter = 0;
            homeScreen.SetActive(true);
            youngScreen.SetActive(false);
            personalRecomendations.SetActive(false);
        }
        youngCounter++;
        youngStepsScreen[youngCounter].SetActive(true);
        youngStepsScreen[youngCounter - 1].SetActive(false);
        progressBarRecomendation[youngCounter].SetActive(true);
        progressBarRecomendation[youngCounter - 1].SetActive(false);
    }
    public void PersonalRecomendationsButton()
    {
        personalRecomendations.SetActive(true);
        if (Convert.ToInt32(PlayerPrefs.GetString("old")) > 64)
        {
            elderlyCounter = 0;
            elderlyStepsScreen[0].SetActive(true);
            elderlyStepsScreen[2].SetActive(false);
            elderlyStepsScreen[1].SetActive(false);
            progressBarRecomendation[0].SetActive(true);
            progressBarRecomendation[1].SetActive(false);
            progressBarRecomendation[2].SetActive(false);
            elderlyScreen.SetActive(true);
        }
        else
        {
            youngCounter = 0;
            youngStepsScreen[0].SetActive(true);
            youngStepsScreen[2].SetActive(false);
            youngStepsScreen[1].SetActive(false);

            progressBarRecomendation[0].SetActive(true);
            progressBarRecomendation[1].SetActive(false);
            progressBarRecomendation[2].SetActive(false);
            youngScreen.SetActive(true);
        }
        homeScreen.SetActive(false);
    }
    public void ExitButtonHomeScreen()
    {
        PlayerPrefs.DeleteAll();
        LoginScreen.SetActive(true);
        homeScreen.SetActive(false);
    }
    public void ExitButtonAllOkey()
    {
        homeScreen.SetActive(true);
        allOkayScreen.SetActive(false);
    }

    public void ExitButtonNeedDoctor()
    {
        homeScreen.SetActive(true);
        needDoctorScreen.SetActive(false);
    }
    public void ExitButtonSpread()
    {
        homeScreen.SetActive(true);
        spreadScreen.SetActive(false);
    }
    public void LoginButton()
    {
        homeScreen.SetActive(true);
        LoginScreen.SetActive(false);
    }

    public void SymptomsTestSlyder()
    {
        if (SymptomsCounter == 2)
        {
            if (symptomsAnswerCounter > 0)
            {
                needDoctorScreen.SetActive(true);
                symptomsScreen.SetActive(false);
            }
            else
            {
                allOkayScreen.SetActive(true);
                symptomsScreen.SetActive(false);
            }
            SymptomsCounter = 0;
        }
        SymptomsCounter++;
        symptomsScreenSteps[SymptomsCounter].SetActive(true);
        symptomsScreenSteps[SymptomsCounter - 1].SetActive(false);
        progressBarTest[SymptomsCounter].SetActive(true);
        progressBarTest[SymptomsCounter - 1].SetActive(false);
    }
    public void SymptomsButton()
    {
        symptomsAnswerCounter = 0;
        SymptomsCounter = 0;
        symptomsScreenSteps[0].SetActive(true);
        symptomsScreenSteps[2].SetActive(false);
        symptomsScreenSteps[1].SetActive(false);
        progressBarTest[0].SetActive(true);
        progressBarTest[2].SetActive(false);
        progressBarTest[1].SetActive(false);
        symptomsScreen.SetActive(true);
        homeScreen.SetActive(false);
    }

    public void SymptomsAnswerCounter()
    {
        symptomsAnswerCounter++;
    }
    public void ProtectSlider()
    {
        if (ProtectCounter == 2)
        {

            ProtectCounter = 0;
            homeScreen.SetActive(true);
            protectScreen.SetActive(false);
        }
        ProtectCounter++;
        protectScreenSteps[ProtectCounter].SetActive(true);
        protectScreenSteps[ProtectCounter - 1].SetActive(false);
        progressBarProtect[ProtectCounter].SetActive(true);
        progressBarProtect[ProtectCounter - 1].SetActive(false);
    }
    public void onProtectButton()
    {
        ProtectCounter = 0;
        protectScreenSteps[0].SetActive(true);
        protectScreenSteps[2].SetActive(false);
        protectScreenSteps[1].SetActive(false);
        progressBarProtect[0].SetActive(true);
        progressBarProtect[2].SetActive(false);
        progressBarProtect[1].SetActive(false);
        protectScreen.SetActive(true);
        homeScreen.SetActive(false);
    }


    public void InfectionSlider()
    {
        if (InfectionCounter == 2)
        {
            InfectionCounter = 0;
            homeScreen.SetActive(true);
            infectedScreen.SetActive(false);
        }
        InfectionCounter++;
        infectedStepsScreen[InfectionCounter].SetActive(true);
        infectedStepsScreen[InfectionCounter - 1].SetActive(false);
        progressBarInfected[InfectionCounter].SetActive(true);
        progressBarInfected[InfectionCounter - 1].SetActive(false);

    }
    public void onInfectionButton()
    {
        InfectionCounter = 0;
        infectedStepsScreen[0].SetActive(true);
        infectedStepsScreen[2].SetActive(false);
        infectedStepsScreen[1].SetActive(false);
        progressBarInfected[0].SetActive(true);
        progressBarInfected[2].SetActive(false);
        progressBarInfected[1].SetActive(false);
        infectedScreen.SetActive(true);
        homeScreen.SetActive(false);
    }


    public void onSpreadButton()
    {
        spreadScreen.SetActive(true);
        homeScreen.SetActive(false);
    }


}
