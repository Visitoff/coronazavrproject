﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class GetJsonData : MonoBehaviour
{
    public GameObject americaInput;
    public Dropdown SelectCountry;
    //   public string selectedCountry;
    string urlForRussia = "https://disease.sh/v3/covid-19/countries/RUS";
    string urlForWorld = "https://disease.sh/v3/covid-19/all";
    string urlForUsa = "https://disease.sh/v3/covid-19/countries/USA";
    string urlForUsaCountries = "https://disease.sh/v3/covid-19/states/";
    [SerializeField]
    Text cases;
    [SerializeField]
    Text recovered;
    [SerializeField]
    Text deaths;
    [SerializeField]
    Text casesToday;
    [SerializeField]
    Text recoveredToday;
    [SerializeField]
    Text deathsToday;
    StateData m_States;
    bool m_DataCaptured;



    public bool dataCaptured
    {
        get => m_DataCaptured;
    }

    public void DropDownSelect()
    {
        if (SelectCountry.value == 0)
            GetAmerica();
        if (SelectCountry.value == 1)
            GetRussia();
        if (SelectCountry.value == 2)
            GetWorld();

    }

    public void GetRussia()
    {
        americaInput.SetActive(false);
        StartCoroutine(GetData(urlForRussia));
    }
    public void GetAmerica()
    {
        StartCoroutine(GetData(urlForUsa));
        americaInput.SetActive(true);
    }
    public void GetWorld()
    {
        americaInput.SetActive(false);
        StartCoroutine(GetData(urlForWorld));
    }
    public void GetAmericaCountry(string selectedCountry)
    {
        if (selectedCountry == "")
            GetAmerica();
        else
        {
            Debug.Log("https://disease.sh/v3/covid-19/states/" + selectedCountry);
            StartCoroutine(GetDataForCountries("https://disease.sh/v3/covid-19/states/" + selectedCountry));
        }
    }
    void Start()
    {
        GetAmerica();
    }

    IEnumerator GetDataForCountries(string url)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(url))
        {
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError)
            {
                Debug.Log("Error occured with web request");
            }
            else
            {

                m_States = JsonConvert.DeserializeObject<StateData>(webRequest.downloadHandler.text);

                cases.text = m_States.cases.ToString();
                recovered.text = m_States.recovered.ToString();
                deaths.text = m_States.deaths.ToString();
                if (m_States.todayCases == null)
                    casesToday.text = "No Info";
                else casesToday.text = m_States.todayCases.ToString();

                if (m_States.todayRecovered == null)
                    recoveredToday.text = "No Info";
                else recoveredToday.text = m_States.todayRecovered.ToString();

                if (m_States.todayDeaths == null)
                    deathsToday.text = "No Info";
                else deathsToday.text = m_States.todayDeaths.ToString();



                Debug.Log(m_States.todayCases);
                Debug.Log(m_States.todayRecovered);
                Debug.Log(m_States.todayDeaths);

            }
        }
    }
    IEnumerator GetData(string url)
    {

        using (UnityWebRequest webRequest = UnityWebRequest.Get(url))
        {
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError)
            {
                Debug.Log("Error occured with web request");
            }
            else
            {

                m_States = JsonConvert.DeserializeObject<StateData>(webRequest.downloadHandler.text);

                cases.text = string.Format("+{0}K", m_States.cases / 1000);
                recovered.text = string.Format("+{0}K", m_States.recovered / 1000);
                deaths.text = string.Format("+{0}K", m_States.deaths / 1000);

                casesToday.text = string.Format("• +{0}", m_States.todayCases);
                recoveredToday.text = string.Format("• +{0}", m_States.todayRecovered);
                deathsToday.text = string.Format("• +{0}", m_States.todayDeaths);

            }
        }
    }
}

public class StateData
{
    public string country;
    public int? cases;
    public int? todayCases;
    public int? deaths;
    public int? todayDeaths;
    public int? recovered;
    public int? todayRecovered;
    public int? active;
    //public string critical;
    //public string dataQualityGrade;
    //public int? negative;
    //public Object pending;
    //public int? hospitalizedCurrently;
    //public Object hospitalizedCumulative;
    //public Object inIcuCurrently;
    //public Object inIcuCumulative;
    //public Object onVentilatorCurrently;
    //public Object onVentilatorCumulative;
    //public int? recovered;
    //public string lastUpdateEt;
    //public string checkTimeEt;
    //public int? death;
    //public Object hospitalized;
    //public int? total;
    //public int? totalTestResults;
    //public int? posNeg;
    //public string fips;
    //public DateTime dateModified;
    //public DateTime dateChecked;
    //public string hash;
}